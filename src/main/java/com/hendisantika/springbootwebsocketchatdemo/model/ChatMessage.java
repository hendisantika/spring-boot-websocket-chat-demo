package com.hendisantika.springbootwebsocketchatdemo.model;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-websocket-chat-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-18
 * Time: 18:55
 * To change this template use File | Settings | File Templates.
 */
public class ChatMessage {
    private MessageType type;
    private String content;
    private String sender;

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }
}
