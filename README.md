# Spring Boot WebSocket Chat Application

#### Requirements

1. Java - 1.8.x
2. Maven - 3.x.x

#### Steps to Setup

1. Clone the application

    `git clone https://gitlab.com/hendisantika/spring-boot-websocket-chat-demo.git`

2. Build and run the app using maven
    ```
    cd spring-boot-websocket-chat-demo
    mvn clean package
    java -jar target/spring-boot-websocket-chat-demo-0.0.1-SNAPSHOT.jar
    ```

    Alternatively, you can run the app directly without packaging it like so -

    `mvn clean spring-boot:run`

#### Screen shot

User 1

![User 1](img/naruto.png "User 1")

User 2

![User 2](img/sasuke.png "User 2")

Chat

![Chat](img/chat.png "Chat")